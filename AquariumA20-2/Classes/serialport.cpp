#include "serialport.h"
#include <QDebug>
#include <QThread>

SerialPort::SerialPort(QObject *parent) :
QSerialPort(parent) {

    //connect(this,SIGNAL(bytesWritten(qint64)),this,SLOT(getMessagetoMulti(char *message)));
    //connect(this,SIGNAL(confirm()),this,SLOT(slotConfirm()));
    messageretour= new char[200];
    //QSplineSeries *courbe = new QSplineSeries();
    //courbe->setName("spline");
    connect(this,SIGNAL(readyRead()),this,SLOT(getMessage()));
}

bool SerialPort::setOpenPort(QString *port){
    //Ouverture du port et configuration du port
    this->setPortName(*port);
    this->close();
        this->setBaudRate(9600);
        this->setDataBits(Data8);
        this->setParity(NoParity);
        this->setStopBits(TwoStop);
       // this->setFlowControl(NoFlowControl);
       // this->setDataTerminalReady(true);
    if (!this->isOpen())
        openport=this->open(QIODevice::ReadWrite);
    else openport=true;
        this->setBaudRate(9600);
        this->setDataBits(Data8);
        this->setParity(NoParity);
        this->setStopBits(TwoStop);
       // this->setFlowControl(NoFlowControl);
       // this->setDataTerminalReady(true);
    messagelu=false;
    return openport;

}

SerialPort::~SerialPort() {
    // TODO Auto-generated destructor stub
    delete []messageretour;
        this->flush(); //If any data was written, this function returns true; otherwise returns false.
    this->close();
}

unsigned char SerialPort::getNextByte(){
    char data;
 this->readData(&data,1);
 return (unsigned char)(data);
}


int SerialPort::writeBuf(char *buf,int l){
    int j=this->writeData(buf,l);
    this->waitForBytesWritten(100);
    this->waitForReadyRead(150);
    return j;
}


bool SerialPort::getOpen(){
    return openport;
}

void SerialPort::setRazMessage(){
    messageretour[0]=0;
}

QByteArray SerialPort::getMessageB(){
	return str;
}


int SerialPort::getMessageVoie(char * voie){

	//while(!messagelu);
	messagelu=false;

    for(int i=0;i<6;i++){
        voie[i]=messageretour[i];
    }
    return longueur;
}

void SerialPort::getMessage(){
    char* messagei=new char[10];
    int l;
   // int j=0;
   // QThread::msleep(5);
   //l= this->readLine(messagei,sizeof(messagei));
   str=this->readAll();
	flush();
//   if (longueur<4){
//         longueur+=l;
//         str1.append(messagei);
//         str+=str1;
//   }
//   else {
       longueur=str.length();
       str.append(messagei);
//   }
   for(int i=0;i<str.length();i++){
	 //  if (str[i]=='#')
	//	   recopie=true;
	 //  if (recopie){
		   messageretour[i]=str.at(i);
		//   j++;
	  // }

     }

  // qDebug()<<"m="<<(int)(messageretour[l])<<"message:"<<messageretour;
}

void SerialPort::getMessagetoMulti(char *message){
//Envoie du message au multimètre
    /*emit pourLire();
    for(int i=0;i<nblue;i++){
        message[i]=messageretour[i];
    }*/
    //qDebug()<<"envoie message";
    this->flush();
    this->write(message);
    this->setDataTerminalReady(true);

}

void SerialPort::setReponse(bool rep){
	reponse =rep;
}

bool SerialPort::getReponse(){
	return messagelu;
}
