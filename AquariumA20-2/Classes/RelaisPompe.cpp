/*
 * RelaisPompe.cpp
 *
 *  Created on: 16 déc. 2017
 *      Author: michel
 */

#include "RelaisPompe.h"

RelaisPompe::RelaisPompe(InterfaceRS232 *portcom,QObject *parent) {
	// TODO Auto-generated constructor stub
	mport=portcom;
/*	   regactionneur=new char[6];
	    regactionneur[0]='#';
	    regactionneur[2]='0';
	    regactionneur[3]=13;
	    regactionneur[1]='E';

*/
    mport->writeBuf(mport->regactionneur,4);
    mport->getMessageVoie(ligne);
}

RelaisPompe::~RelaisPompe() {


	// TODO Auto-generated destructor stub
}

void RelaisPompe::commandeRelais(bool commandepompe){
    if (commandepompe){
     mport->regactionneur[2]|=1;
     commandepompe=false;
    }
    else{
        mport->regactionneur[2]&=0xFE;
        commandepompe=true;
    }
    mport->writeBuf(mport->regactionneur,4);
    mport->getMessageVoie(ligne);
}

void RelaisPompe::lireMessage(char *l){
	for (int i=0;i<6;i++)
		l[i]=ligne[i];
}


