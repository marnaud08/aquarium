/**
 *  Copyright (C) 2017  michel  (michel@btssn.delattre.com)
 *  @file         Aquarium.cpp
 *  @brief        
 *  @version      0.1
 *  @date         29 déc. 2017 23:26:01
 *
 *  Description detaillee du fichier Aquarium.cpp
 *  Fabrication   gcc (Ubuntu 5.4.0-6ubuntu1~16.04.5) 5.4.0 20160609 
 *  @todo         Liste des choses restant a faire.
 *  @bug          29 déc. 2017 23:26:01 - Aucun pour l'instant
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
//#define PORT "/dev/ttyS2"
#define PORT "/dev/ttyS2"
// Includes system C

// Includes system C++
// A décommenter si besoin cout, cin, ...
// #include <iostream>
// using namespace std;

// Includes qt

// Includes application
#include "Aquarium.h"

/**
 * Constructeur
 */
Aquarium::Aquarium()
{
	this->setupUi(this);
	mport=new InterfaceRS232(this);
    QString port=PORT;
    mport->setOpenPort(&port);
   mcapteur = new Capteur(mport,this);
   mchauffage = new RelaisTemperature(mport,this);
   this->mpompe=new RelaisPompe(mport,this);
 //  mmiseajour = new Battement(this);
 //  connect(this->mmiseajour,SIGNAL(miseajour()),this,SLOT(supervision()));
   QColor vert(Qt::green);
   QPalette palette;
   palette.setColor(QPalette::Button,QColor(Qt::red));
   commandepompe=true;
   this->Relais_Temperature->setPalette(palette);
   this->Relais_Pompe->setStyleSheet("QPushButton {"
           "background-color: blue;"
           "border-style: outset;"
           "border-width: 2px;"
         "border-radius: 10px;"
          "border-color: beige;"
           "font: bold 14px;"
           "min-width: 10em;"
           "padding: 6px;"
       "}");
   this->Relais_Temperature->setStyleSheet("QPushButton { background-color: blue; }");
   consigneteau=15;
	m_bd=new BaseDonnees(this);
	m_bd->connecter("BDDloutre");
  mmiseajour = new Battement(this);
  qDebug()<<"battement lancer";
   connect(this->mmiseajour,SIGNAL(miseajour()),this,SLOT(supervision()));
   qDebug()<<"connect  fait";
    mmiseajour->startb();	
  qDebug()<<"programme lancer";

}

/**
 * Destructeur
 */
Aquarium::~Aquarium()
{
	delete mcapteur;
	delete mchauffage;
	delete mpompe;
	mmiseajour->quit();
	if(!mmiseajour->wait(3000)) //Wait until it actually has terminated (max. 3 sec)
	{
	    mmiseajour->terminate(); //Thread didn't exit in time, probably deadlocked, terminate it!
	    mmiseajour->wait(); //We have to wait again here!
	}

	delete mmiseajour;
	delete mport;
	delete m_bd;
}

void Aquarium::supervision(){
	   char ligne[10];
 QString texte;int teau;int tamb;int l;int niv;int ph;
 double niveau;
	//int ta=mtempambiante->lireCapteur(1);
	mcapteur->lireCapteur(VOIETEAU,&teau);

	double valtempeau=5*teau/(4.096*TC1)-VOT/TC1;
	this->lineTempEau->setText(texte.setNum(valtempeau));


	mcapteur->lireCapteur(VOIEPH,&ph);
	double valph=(double)(ph)*17.5/4096+PHOFFSET;
	this->linePH->setText(texte.setNum(valph));

	mcapteur->lireCapteur(VOIENIVEAUEAU,&niv);
	if (niv>2000)
		this->lineNivh2o->setText("Non atteind");
	else this->lineNivh2o->setText("atteind");


	mcapteur->lireCapteur(VOIETAMBIANTE,&tamb);
	double valtempamb=5*tamb/(4.096*TC1)-VOT/TC1;
	this->lineTempAmb->setText(texte.setNum(valtempamb));
	//this->lineNivh2o->setText(texte.setNum((niv)));

	mcapteur->lireCapteur(VOIELUMINOSITE,&l);
	//mcapteur->lireCapteur(VOIELUMINOSITE,&l);
	double luminosite=l*4.02832/RLUMIERE;
	this->lineLumiere->setText(texte.setNum(luminosite));

	if (this->consigneteau>valtempeau){
		this->Relais_Temperature->setStyleSheet("QPushButton { background-color: red; }");
		mchauffage->commandeRelais(true);
		niveau=0;
	}
	else {
		this->Relais_Temperature->setStyleSheet("QPushButton { background-color: green; }");
		mchauffage->commandeRelais(false);
		niveau=1.0;
	}
	m_bd->inserer(valtempamb,valtempeau,niveau,valph,luminosite);

}

/*
void Aquarium::on_Lire_clicked(){
	   char ligne[10];
    QString texte;int teau;int tamb;int l;int niv;int ph;
	//int ta=mtempambiante->lireCapteur(1);
  while (!mcapteur->lireCapteur(VOIETEAU,&teau));
  while (!mcapteur->lireCapteur(VOIETEAU,&teau));

	double valtempeau=5*teau/(4.096*TC1)-VOT/TC1;
	this->lineTempEau->setText(texte.setNum(valtempeau));


	while(!mcapteur->lireCapteur(VOIEPH,&ph));
	//mcapteur->lireCapteur(VOIEPH,&ph);
	double valph=(double)(ph)*17.5/4096+PHOFFSET;
	this->linePH->setText(texte.setNum(valph));

	while(!mcapteur->lireCapteur(VOIENIVEAUEAU,&niv));
	//mcapteur->lireCapteur(VOIENIVEAUEAU,&niv);
	if (niv>2000)
		this->lineNivh2o->setText("Non atteind");
	else this->lineNivh2o->setText("atteind");


	while(!mcapteur->lireCapteur(VOIETAMBIANTE,&tamb));
	double valtempamb=5*tamb/(4.096*TC1)-VOT/TC1;
	this->lineTempAmb->setText(texte.setNum(valtempamb));
	//this->lineNivh2o->setText(texte.setNum((niv)));
	while(!mcapteur->lireCapteur(VOIELUMINOSITE,&l));
	//mcapteur->lireCapteur(VOIELUMINOSITE,&l);
	double luminosite=l*4.02832/RLUMIERE;
	this->lineLumiere->setText(texte.setNum(luminosite));

	if (this->consigneteau>valtempeau){
		this->Relais_Temperature->setStyleSheet("QPushButton { background-color: red; }");
		mchauffage->commandeRelais(true);
	}
	else {
		this->Relais_Temperature->setStyleSheet("QPushButton { background-color: green; }");
		mchauffage->commandeRelais(false);
	}

}
*/
void Aquarium::on_Relais_Pompe_clicked(){
    char ligne[10];
    mpompe->commandeRelais(commandepompe);
    if (commandepompe){
    	commandepompe=false;
  //  	   this->Relais_Pompe->setStyleSheet("QPushButton { background-color: green; }");

        this->Relais_Pompe->setText("Arrêt Pompe");
    }
    else{
    	commandepompe=true;
 //	   this->Relais_Pompe->setStyleSheet("QPushButton { background-color: black; }");
        this->Relais_Pompe->setText("Commande Pompe");
    }
  //  mpompe->lireMessage(ligne);
   // this->lineNivh2o->setText(ligne);
    //QString texte;
    //this->lineLumiere->setText(texte.setNum((unsigned char)(ligne[2]-0x80)));
}




void Aquarium::on_ConsigneTeau_sliderReleased(){
	consigneteau=this->ConsigneTeau->value();
	QString text;
	this->lineConsigneEau->setText(text.setNum(consigneteau));
}
