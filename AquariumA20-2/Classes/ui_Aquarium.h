/********************************************************************************
** Form generated from reading UI file 'Aquarium.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_AQUARIUM_H
#define UI_AQUARIUM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDial>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Aquarium
{
public:
    QWidget *centralwidget;
    QLineEdit *lineTempEau;
    QPushButton *Relais_Temperature;
    QLineEdit *lineTempAmb;
    QLineEdit *lineNivh2o;
    QLineEdit *linePH;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLineEdit *lineLumiere;
    QLabel *label_5;
    QPushButton *Relais_Pompe;
    QDial *ConsigneTeau;
    QLabel *label_6;
    QLineEdit *lineConsigneEau;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *Aquarium)
    {
        if (Aquarium->objectName().isEmpty())
            Aquarium->setObjectName(QStringLiteral("Aquarium"));
        Aquarium->resize(648, 458);
        centralwidget = new QWidget(Aquarium);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        lineTempEau = new QLineEdit(centralwidget);
        lineTempEau->setObjectName(QStringLiteral("lineTempEau"));
        lineTempEau->setEnabled(false);
        lineTempEau->setGeometry(QRect(10, 130, 113, 27));
        Relais_Temperature = new QPushButton(centralwidget);
        Relais_Temperature->setObjectName(QStringLiteral("Relais_Temperature"));
        Relais_Temperature->setEnabled(false);
        Relais_Temperature->setGeometry(QRect(140, 190, 111, 27));
        lineTempAmb = new QLineEdit(centralwidget);
        lineTempAmb->setObjectName(QStringLiteral("lineTempAmb"));
        lineTempAmb->setEnabled(false);
        lineTempAmb->setGeometry(QRect(140, 130, 113, 27));
        lineNivh2o = new QLineEdit(centralwidget);
        lineNivh2o->setObjectName(QStringLiteral("lineNivh2o"));
        lineNivh2o->setEnabled(false);
        lineNivh2o->setGeometry(QRect(260, 130, 113, 27));
        linePH = new QLineEdit(centralwidget);
        linePH->setObjectName(QStringLiteral("linePH"));
        linePH->setEnabled(false);
        linePH->setGeometry(QRect(390, 130, 113, 27));
        label = new QLabel(centralwidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(30, 100, 67, 17));
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(140, 100, 111, 20));
        label_3 = new QLabel(centralwidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(290, 100, 67, 17));
        label_4 = new QLabel(centralwidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(420, 100, 67, 17));
        lineLumiere = new QLineEdit(centralwidget);
        lineLumiere->setObjectName(QStringLiteral("lineLumiere"));
        lineLumiere->setEnabled(false);
        lineLumiere->setGeometry(QRect(520, 130, 113, 27));
        label_5 = new QLabel(centralwidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(526, 100, 111, 20));
        Relais_Pompe = new QPushButton(centralwidget);
        Relais_Pompe->setObjectName(QStringLiteral("Relais_Pompe"));
        Relais_Pompe->setEnabled(true);
        Relais_Pompe->setGeometry(QRect(260, 190, 111, 27));
        ConsigneTeau = new QDial(centralwidget);
        ConsigneTeau->setObjectName(QStringLiteral("ConsigneTeau"));
        ConsigneTeau->setGeometry(QRect(140, 283, 101, 91));
        ConsigneTeau->setMinimum(15);
        ConsigneTeau->setMaximum(30);
        label_6 = new QLabel(centralwidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(140, 250, 111, 17));
        lineConsigneEau = new QLineEdit(centralwidget);
        lineConsigneEau->setObjectName(QStringLiteral("lineConsigneEau"));
        lineConsigneEau->setEnabled(true);
        lineConsigneEau->setGeometry(QRect(140, 380, 113, 27));
        Aquarium->setCentralWidget(centralwidget);
        menubar = new QMenuBar(Aquarium);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 648, 25));
        Aquarium->setMenuBar(menubar);
        statusbar = new QStatusBar(Aquarium);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        Aquarium->setStatusBar(statusbar);

        retranslateUi(Aquarium);

        QMetaObject::connectSlotsByName(Aquarium);
    } // setupUi

    void retranslateUi(QMainWindow *Aquarium)
    {
        Aquarium->setWindowTitle(QApplication::translate("Aquarium", "Test de la base de donn\303\251es", 0));
        Relais_Temperature->setText(QApplication::translate("Aquarium", "Temperature", 0));
        label->setText(QApplication::translate("Aquarium", "TempEau", 0));
        label_2->setText(QApplication::translate("Aquarium", "TempAmbiante", 0));
        label_3->setText(QApplication::translate("Aquarium", "Nivh2o", 0));
        label_4->setText(QApplication::translate("Aquarium", "PH", 0));
        label_5->setText(QApplication::translate("Aquarium", "Lumiere (Lux)", 0));
        Relais_Pompe->setText(QApplication::translate("Aquarium", "Pompe", 0));
        label_6->setText(QApplication::translate("Aquarium", "Consigne teau", 0));
    } // retranslateUi

};

namespace Ui {
    class Aquarium: public Ui_Aquarium {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_AQUARIUM_H
