/*
 * Capteur.cpp
 *
 *  Created on: 21 déc. 2016
 *      Author: michel
 */

#include "Capteur.h"
#include <QDebug>

Capteur::Capteur(SerialPort *portcom,QObject *parent):QObject(parent) {
	// TODO Auto-generated constructor stub
	mport=portcom;
	   regcapteur=new char[6];
	    regcapteur[0]='#';
	    regcapteur[2]='1';
	    regcapteur[3]=13;
	    regcapteur[1]='L';

}

Capteur::~Capteur() {
	// TODO Auto-generated destructor stub
	//delete mmk2;
}

bool Capteur::lireCapteur(int voie,int *val){
	regcapteur[2]='0'+voie;
    mport->writeBuf(regcapteur,4);
    int l=mport->getMessageVoie(ligne);
    qDebug()<<"l="<<l;
    *val= ((((unsigned char)(ligne[3]))<<8)+(unsigned char)(ligne[4]));
    if (ligne[0]=='#')
    	return true;
    return false;

}

void Capteur::lireMessage(char *l){
	for (int i=0;i<6;i++)
		l[i]=ligne[i];
}
