/*
 * Capteur.h
 *
 *  Created on: 21 déc. 2016
 *      Author: michel
 */

#ifndef CLASSES_CAPTEUR_H_
#define CLASSES_CAPTEUR_H_
#include "serialport.h"
#include <QObject>
#define VOIETEAU 0
#define VOIETAMBIANTE 1
#define VOIEPH 4
#define VOIELUMINOSITE 8
#define VOIENIVEAUEAU 10
#define TC1 19.8
#define VOT 400
#define PHOFFSET 0
#define RLUMIERE 68


class Capteur : public QObject{
		Q_OBJECT
		SerialPort *mport;
	    char *regcapteur;
	    char ligne[10];;

public:
	Capteur(SerialPort *,QObject *parent=0);
	virtual ~Capteur();
	bool lireCapteur(int voie,int *val);
	void lireMessage(char *);
};

#endif /* CLASSES_CAPTEUR_H_ */
